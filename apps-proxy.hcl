job "infra-proxy-apps" {

  datacenters = ["dc1"]

  task "apps-proxy" {
    driver = "docker"

    config {
      image = "rolandjohann/consul-template-nginx:latest"

      port_map {
        nginx = 80
      }

      network_mode = "host"
    }

    resources {
      cpu = 4096
      memory = 2048
      network {
        mbits = 10
        port "nginx" { static = 80 }
      }
    }

    service {
      name = "infra-proxy-apps"
      port = "nginx"
//      check {
//        type = "script"
//        command = "/usr/bin/curl"
//        args = ["localhost"]
//        interval = "10s"
//        timeout = "1s"
//      }
    }
  }
}
